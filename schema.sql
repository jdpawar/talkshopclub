CREATE DATABASE talkshopclub;

CREATE TABLE users (
	id uuid PRIMARY KEY NOT NULL,
	username text NOT NULL,
	full_name text,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);

CREATE TABLE posts (
	id uuid PRIMARY KEY NOT NULL,
    user_id uuid REFERENCES users (id) NOT NULL,
	post_type text NOT NULL,
	caption text,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);

CREATE TABLE post_analysis (
    post_id uuid REFERENCES posts (id) PRIMARY KEY NOT NULL,
	word_count int NOT NULL,
	avg_word_length int NOT NULL,
	impression_count int NOT NULL,
	like_count int NOT NULL,
	comment_count int NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL
);

-- Sample data

INSERT INTO users VALUES ('416f9db7-587d-46d4-9892-0e7dbe295534', 'jdpawar', 'JD Pawar', NOW(), NOW());