const { body, validationResult } = require('express-validator');

// validate POST request body for creating post
const validateCreatePost = [
  body('id').isUUID().notEmpty(),
  // body('user_id').isUUID().notEmpty(), // TO be used from auth token
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  },
];

module.exports = { validateCreatePost };
