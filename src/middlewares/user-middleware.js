const config = require('config');
var jwt = require('jsonwebtoken');

const requireUser = async (req, res, next) => {
  const token = req.headers.authorization.replace('Bearer ', '');
  var decoded = jwt.verify(token, config.get('jwt.secret'));
  if (!decoded) {
    return res.status(401).json();
  }

  // TODO: Cache the user details in redis and serve it from redis, for now using it from the token
  req.user = {
    id: decoded.data.user_id,
    username: decoded.data.username,
  }

  next();

  // TODO: Ability to invilidate the token

  // sample code to create JWT for a user
  // const payload = {
  //   iss: 'talkshopclub.com',
  //   typ: 'JWT',
  //   alg: 'HS256',
  //   data: {
  //     user_id: '416f9db7-587d-46d4-9892-0e7dbe295534',
  //     username: 'jdpawar'
  //   }
  // }
  // const token = jwt.sign(payload, config.get('jwt.secret'))
  // console.log(token);
}

module.exports = { requireUser }