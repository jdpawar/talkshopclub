process.env.NODE_CONFIG_DIR = `${__dirname}/../config`;
const config = require('config');
const { KafkaConsumer } = require('../config/kafka');
const { getCachedPostAnalysis } = require('../cache/redis-post-analysis');

const consumer = new KafkaConsumer(
  config.get('kafkaTopics.postAnalysis'),
  config.get('kafkaConsumerGroupIds.postAnalysis'),
  false,
);

const consumeEvents = async () => {

  await consumer.connect();

  consumer.readEachMessage(async ({ message }) => {
    try {

      const postId = JSON.parse(message.value.toString());
      console.log('Post Analysis Consumer - Post ID', postId);

      postAnalysis = getCachedPostAnalysis(postId);
      if (!postAnalysis) {
        console.log('Post Analysis Consumer - Failed to update post analysis')
      }

    } catch (e) {
      console.log('Post Analysis Consumer - Failed to update post analysis', e)
    }
  });
}

consumeEvents();