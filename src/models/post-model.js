const db = require('../config/db');
const dbReplica = require('../config/db-replica');
const { averageWordLength, wordCount } = require('../utils/word-utils');

// create a new post
async function getPostAnalysis(id) {

  const post = await getPostById(id);
  if (post == null) {
    throw 'Post not found';
  }

  var wCount = wordCount(post.caption);
  var avgWordLength = averageWordLength(post.caption);
  var impressionCount = Math.floor(Math.random() * 1000);
  var likeCount = Math.floor(Math.random() * 1000);
  var commentCount = Math.floor(Math.random() * 1000);

  try {

    const analysis = await db.one(
      `
        INSERT INTO post_analysis
          (post_id, word_count, avg_word_length, impression_count, like_count, comment_count, updated_at) 
        VALUES 
          ($1, $2, $3, $4, $5, $6, NOW())
        ON CONFLICT (post_id) DO UPDATE
        SET word_count = $2, avg_word_length = $3, impression_count = $4, like_count = $5, comment_count = $6, updated_at = NOW()
        returning *
      `,
      [id, wCount, avgWordLength, impressionCount, likeCount, commentCount]
    );

    return analysis;

  } catch (error) {
    console.error(`Error updating post analytics for post ID: ${id}`, error);
    throw error;
  }
}

// Get a post by ID
async function getPostById(id) {
  try {
    const post = await dbReplica.oneOrNone(
      `SELECT * FROM posts WHERE id = $1`,
      [id]
    );

    return post;

  } catch (error) {
    throw error;
  }
}

// Delete a post by ID
async function deletePostById(id) {

}

module.exports = { getPostById, deletePostById, getPostAnalysis };
