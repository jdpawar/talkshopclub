const express = require('express');
const router = express.Router();
const { requireUser } = require('../middlewares/user-middleware');
const { validateCreatePost } = require('../middlewares/validation-middleware');
const PostController = require('../controllers/post-controller');

// post routes
router.post('/posts', requireUser, validateCreatePost, PostController.createPost);
router.get('/posts/:id', PostController.getPost);
router.get('/posts/:id/analysis', PostController.getPostAnalysis);
// router.put('/post/:id', PostController.updatePost);
// router.delete('/post/:id', PostController.deletePost);

module.exports = router;