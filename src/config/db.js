const promise = require('bluebird');
const pgPromise = require('pg-promise');
const config = require('config');
const os = require('os');

promise.config({
  longStackTraces: true,
});

const initOptions = {
  promiseLib: promise,
  error(error, e) {
    console.error(e.query);
    error.query = e.query;
    error.DB_ERROR = true;
    return { ...error, DB_ERROR: true };
  },
};

const pgp = pgPromise(initOptions);

const dbConfig = {
  host: config.get('db.host'),
  port: config.get('db.port'),
  database: config.get('db.database'),
  user: config.get('db.user'),
  password: config.get('db.password'),
  application_name: process.env.SERVICE_NAME ? process.env.SERVICE_NAME : os.userInfo().username
};

const db = pgp(dbConfig);

module.exports = db;