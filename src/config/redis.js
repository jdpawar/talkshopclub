const Redis = require('ioredis');
const config = require('config');

const ioRedis = new Redis({
  ...config.get('redis'),
  db: 0,
});

module.exports = ioRedis;