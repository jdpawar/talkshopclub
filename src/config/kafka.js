const config = require('config');
const { Kafka, CompressionTypes } = require('kafkajs');

class KafkaProducer {
  constructor(
    topic,
    partitions = 1,
    replicationFactor,
    maxInFlightRequests = 1,
    idempotent = true
  ) {

    this.topic = topic;
    this.partitions = partitions;
    this.maxInFlightRequests = !maxInFlightRequests ? 5 : maxInFlightRequests;
    this.logger = console;
    this.isConnected = false;

    this.logger.debug(`Kafka - Producer ${this.topic}`);

    this.kafka = new Kafka({
      connectionTimeout: 5000,
      authenticationTimeout: 5000,
      clientId: config.get('kafka.clientId'),
      brokers: config.get('kafka.brokers'),
      logger: this.logger,
    });

    this.producer = this.kafka.producer({
      maxInFlightRequests: this.maxInFlightRequests,
      retry: {
        initialRetryTime: 100,
        retries: 8,
      },
    });
  }

  async connect() {
    if (!this.isConnected) {
      this.logger.debug(`Kafka - Connecting`);
      await this.producer.connect(); // TODO: Error handling
      this.logger.debug(`Kafka - Connected`);
      this.isConnected = true;
    }
  }

  async send(message, key = null) {
    if (!this.isConnected) {
      this.logger.debug(`Kafka - Not connected`);
    }

    const keyParam = typeof key === 'string' ? { key } : {};
    const messageParam = [{ value: JSON.stringify(message), ...keyParam }];

    await this.producer
      .send({
        topic: this.topic,
        messages: messageParam,
        acks: - 1,
        compression: CompressionTypes.GZIP,
      })
      .catch(async (e) => {
        this.logger.error(`Kafka - Could not produce single message ${this.topic} ${messageParam}`, e);

        // TODO: Write such messages to DB to process them later
        // INSERT INTO kafka_fallback (id, topic, type (single / bulk), key, message, created_at) VALUES (...)
      });
  }

  async sendBulk(messages) {
    if (!this.isConnected) {
      this.logger.debug(`Kafka - Not connected`);
    }

    await this.producer
      .send({
        topic: this.topic,
        messages, acks: -1,
        compression: CompressionTypes.GZIP
      })
      .catch(async (e) => {
        this.logger.error(`Kafka - Could not produce bulk messages ${this.topic} ${messages}`, e);
        // TODO: Write such messages to DB to process them later
        // INSERT INTO kafka_fallback (id, topic, type (single / bulk), key, message, created_at) VALUES (...)

      });
  }
}

class KafkaConsumer {
  constructor(
    topic,
    groupId,
    fromBeginning,
    sessionTimeout = 30000
  ) {
    
    this.topic = topic;
    this.groupId = groupId;
    this.fromBeginning = fromBeginning;
    this.sessionTimeout = sessionTimeout;
    this.logger = console
    this.logger.debug(`Kafka - Consumer Topic: ${this.topic} Group ID: ${this.groupId}`);

    this.kafka = new Kafka({
      connectionTimeout: 5000,
      authenticationTimeout: 5000,
      clientId: config.get('kafka.clientId'),
      brokers: config.get('kafka.brokers'),
      logger: this.logger
    });

    this.admin = this.kafka.admin();

    this.consumer = this.kafka.consumer({
      groupId: this.groupId,
      sessionTimeout: this.sessionTimeout,
    });
  }

  async connect() {
    // Connect via admin to check if this topic is created or not
    await this.admin.connect();
    const topics = await this.admin.listTopics();

    // When consumer starts before the producer
    // If the topic is not created, create one
    if (topics.indexOf(this.topic) === -1) {
      await this.admin.createTopics({
        waitForLeaders: true,
        topics: [{ topic: this.topic }],
      });
    }

    await this.consumer.connect();
    await this.consumer.subscribe({
      topic: this.topic,
      fromBeginning: this.fromBeginning ? true : false,
    });

    this.logger.debug(`Kafka - Consumer connected - Topic: ${this.topic} Group ID: ${this.groupId}`);
  }

  // Read single message
  async readEachMessage(cb, autoCommit = true) {
    await this.consumer.run({
      eachMessage: cb,
      autoCommit,
    });
  }

  // Read bulk messages
  async readBatch(cb) {
    await this.consumer.run({
      eachBatchAutoResolve: true,
      eachBatch: cb,
    });
  }

  // async commitOffsets(partition, offset) {
  //   await this.consumer.commitOffsets([
  //     { topic: this.topic, partition, offset },
  //   ]);
  // }

  // async disconnect() {
  //   this.logger.debug(`Kafka - Consumer disconnecting - Topic: ${this.topic} Group ID: ${this.groupId}`);
  //   await this.consumer.disconnect();
  // }
}

module.exports = { KafkaProducer, KafkaConsumer };
