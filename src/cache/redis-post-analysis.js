const redis = require('../config/redis');
const { getPostAnalysis: fetchPostAnalysis } = require('../models/post-model');

const TTL_IN_SEC = 3 * 60 * 60;

// Get and/or update post analysis
async function getCachedPostAnalysis(postId) {
  const cacheKey = `post_analysis:${postId}`;

  const cachedData = await redis.get(cacheKey);

  if (cachedData) {
    
    console.log('Redis post analysis - using data from cache');
    return JSON.parse(cachedData);

  } else {
    
    const analyticsData = await fetchPostAnalysis(postId);
    await redis.set(cacheKey, JSON.stringify(analyticsData), 'ex', TTL_IN_SEC);
    
    return analyticsData;
  }
}

module.exports = { getCachedPostAnalysis }