function averageWordLength(text) {
  text = text.trim();
  if (!text || text.length == 0) {
    return 0;
  }
  const words = text.split(/\s+/); // Split by whitespace (space, tab, newline, etc.)
  const totalLength = words.reduce((acc, word) => acc + word.length, 0);
  const averageLength = words.length > 0 ? totalLength / words.length : 0;
  return averageLength;
}

function wordCount(text) {
  text = text.trim();
  if (!text || text.length == 0) {
    return 0;
  }
  return text.replace(/-/g, ' ').trim().split(/\s+/g).length;
}

module.exports = { averageWordLength, wordCount }