process.env.NODE_CONFIG_DIR = `${__dirname}/config`;
const http = require('http');
const express = require('express');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const { v4: uuidV4 } = require('uuid');

const postRoutes = require('./routes/posts-routes');

const SERVER_PORT = 3000;
const serverUUID = uuidV4();

const startHttpServer = async function () {
  const app = express();
  app.use(cookieParser());
  app.use(compression());
  app.use(express.json());
  app.use(
    express.urlencoded({
      extended: true,
    }),
  );

  app.get('/', async (req, res) => {
    res.send('Server is healthy');
  });

  app.use('/api/v1', postRoutes);

  const server = http.createServer(app).listen(SERVER_PORT, () => {
    console.log(`Created server ${serverUUID} listening on port ${SERVER_PORT} ${Date.now()}`);
  });

  // server.keepAliveTimeout = 76 * 1000;
  // server.headersTimeout = 80 * 1000;

  return server;
};

const startTime = new Date();
startHttpServer().then(async () => {
  console.log(`HTTP server ${serverUUID} started. Time taken: ${new Date() - startTime}`,
  );
});

['SIGINT', 'SIGTERM', 'SIGUSR2'].forEach((signal) =>
  process.on(signal, () => setTimeout(process.exit(0), 5000)),
);