const config = require('config');
const db = require('../config/db');
const dbReplica = require('../config/db-replica');
const { getCachedPostAnalysis } = require('../cache/redis-post-analysis');
const { KafkaProducer } = require('../config/kafka');

// All the raw queries can be shifted to ../models with proper schema
// But as per my experience, raw queries scale better

class PostController {

  // Create a new post
  async createPost(req, res) {
    try {
      // Data is already validated via middleware
      const { id, caption, user_id, trace_id } = req.body;
      const post_type = 'TEXT'; // For now assuming only text post. Can be IMAGE, IMAGES, VIDEO, etc.

      // Create post out of this data
      const newPost = await db.oneOrNone(
        `INSERT INTO posts (id, user_id, post_type, caption, updated_at) VALUES($1, $2, $3, $4, NOW()) returning *`,
        [id, req.user.id, post_type, caption]
      );

      const kafkaProducer = new KafkaProducer(config.get('kafkaTopics.postAnalysis'));
      await kafkaProducer.connect();
      await kafkaProducer.send(req.body.id);

      return res.status(201).json(newPost);

    } catch (error) {
      console.log(error);
      console.error('Error creating post:');
      return res.status(500).json({ error: 'Internal server error' });
    }
  }

  // Get a post by ID
  async getPost(req, res) {
    try {
      const { id } = req.params;

      const post = await dbReplica.oneOrNone(
        `SELECT * FROM posts WHERE id = $1`,
        [id]
      );


      if (post) {
        return res.status(200).json(post);
      } else {
        return res.status(404).json({ error: 'Post not found' });
      }
    } catch (error) {
      console.error('Error fetching post:', error);
      return res.status(500).json({ error: 'Internal server error' });
    }
  }

  async getPostAnalysis(req, res) {
    try {
      const { id } = req.params;

      const postAnalysis = await getCachedPostAnalysis(id);

      if (postAnalysis) {
        return res.status(200).json(postAnalysis);
      } else {
        return res.status(404).json({ error: 'Analysis work in progress' });
      }
    } catch (error) {
      console.error('Error fetching post:', error);
      return res.status(500).json({ error: 'Internal server error' });
    }
  }

  // Update a post by ID
  // Delete a post by ID
}

module.exports = new PostController();